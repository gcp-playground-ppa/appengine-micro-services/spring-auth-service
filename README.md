# spring-auth-service
NB : Spring security OAuth is deprecated, but a new comunity one is to be created : 
https://github.com/spring-projects-experimental/spring-authorization-server

made with help from 
https://www.baeldung.com/sso-spring-security-oauth2

# Use Locally
1. Compile `mvn clean package`
2. Start server `java -jar target\spring-auth-service-0.0.1-SNAPSHOT.jar`
3. This microservice is deployed with default profile "dev" to localhost port 8083. Login URL for local try : 
http://localhost:8083/oauth/authorize?response_type=code&&scope=write%20read&client_id=newClient&redirect_uri=http://localhost:3000/login/

# Deploy to Google Cloud App Engine

1.  Compile : `mvn clean package -Pgcp`
2.  Connect to your GCP account, verify that API Cloud Compile is enabled.
4.  Deploy to GCP : `mvn appengine:deploy`

# Eléménts de context : 
- L'objectif du projet est d'explorer les possibilités de Google Cloud Always Free. 
- La base de donnée retenue est Google Cloud Firestore qui propose un quota gratuit.
- La base de donnée Firestore est de type NoSql et non compatible avec JPA. le repository utilisé est donc FirestoreReactiveRepository.

# TODO : 
- Personnalisation de la page de login
- Utilisation token JWT pour compatibilité Spring Security 5 (client side)