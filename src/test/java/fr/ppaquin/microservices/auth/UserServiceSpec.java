package fr.ppaquin.microservices.auth;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.cloud.gcp.data.firestore.FirestoreReactiveRepository;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import fr.ppaquin.microservices.auth.dao.UserRepository;
import fr.ppaquin.microservices.auth.entity.User;
import fr.ppaquin.microservices.auth.services.GcpUserServiceImpl;
import reactor.core.publisher.Mono;

@SpringBootTest(classes = SpringAuthServiceApplication.class)
public class UserServiceSpec {

	@Autowired private GcpUserServiceImpl userService;

	@MockBean private UserRepository userRepository;

	@Test
	void shouldImplementsAuthService() throws Exception {
		assertThat(userService).isInstanceOf(UserDetailsService.class);
		assertThat(userRepository).isInstanceOf(FirestoreReactiveRepository.class);
	}

	@Test
	void shouldListUsers() throws Exception {
		when(userRepository.findById("toto")).thenReturn(Mono.just(new User("toto", "encodedPassword")));

		UserDetails user = userService.loadUserByUsername("toto");

		verify(userRepository, times(1)).findById("toto");
		assertThat(user.getUsername()).isEqualTo("toto");
		assertThat(user.getPassword()).isEqualTo("encodedPassword");
		assertThat(user.getAuthorities().size()).isEqualTo(1);
		assertThat(user.getAuthorities().iterator().next().getAuthority()).isEqualTo("ROLE_USER");
	}

	@Test
	void shouldThrowUsernameNotFoundException() throws Exception {
		when(userRepository.findById("toto")).thenReturn(Mono.empty());

		boolean errorThrown = false;
		try {
			userService.loadUserByUsername("toto");
			fail();
		} catch (UsernameNotFoundException e) {
			errorThrown = true;
		}

		assertTrue(errorThrown);

	}

}
