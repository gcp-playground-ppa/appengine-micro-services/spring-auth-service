package fr.ppaquin.microservices.auth.dao;

import org.springframework.cloud.gcp.data.firestore.FirestoreReactiveRepository;

import fr.ppaquin.microservices.auth.entity.User;
import reactor.core.publisher.Flux;

public interface UserRepository extends FirestoreReactiveRepository<User> {

	Flux<User> findByName(String name);
}
