package fr.ppaquin.microservices.auth.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;

@Configuration
@Order(1)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired //
	@Qualifier("userDetailsService") //
	private UserDetailsService userDetailsService;

	@Override
	protected void configure(HttpSecurity http) throws Exception { // @formatter:off
        http.requestMatchers()
        	.antMatchers("/login", "/oauth/authorize")
        	.and()
        	.authorizeRequests()
        	.anyRequest().authenticated()
        	.and()
        	.formLogin().permitAll()
        	//.loginPage("/login") // Une personnalisation de la page de login sera faite prochainement
        	.and().csrf().disable();
	}
	// @formatter:on

	@Override
	protected UserDetailsService userDetailsService() {
		return userDetailsService;
	}
}