package fr.ppaquin.microservices.auth.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import fr.ppaquin.microservices.auth.dao.UserRepository;
import fr.ppaquin.microservices.auth.entity.User;

@Service("userDetailsService")
public class GcpUserServiceImpl implements UserDetailsService {

	@Autowired private UserRepository userRepository;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		User user = userRepository.findById(username).block();
		UserDetails userDetails = null;
		if (user != null) {
			userDetails = org.springframework.security.core.userdetails.User.builder()
																			.username(user.getName())
																			.password(user.getPassword())
																			.roles("USER")
																			.build();
		} else {
			throw new UsernameNotFoundException(username + " is not found ");
		}

		return userDetails;
	}

}
